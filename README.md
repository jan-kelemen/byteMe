# p-p-i-j
Project for Programming Paradigms and Languages course.

## Task description
Task was to make a educational application. Our choice was a synthesizer simulator.

## Group members
* [Stribor L. Gruičić, 0036478067](https://github.com/Bibo616)
* [Jan Kelemen, 0036479753](https://github.com/jan-kelemen)
* [Domagoj Latečki, 0036478777](https://github.com/dl1994)
* [Alen Murtić, 0036478740](https://github.com/murta6)
* [Borna Skukan, 0036480894](https://github.com/skuxy)
