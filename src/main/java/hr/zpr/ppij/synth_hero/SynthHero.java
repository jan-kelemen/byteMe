package hr.zpr.ppij.synth_hero;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import com.jogamp.opengl.util.Animator;
import hr.zpr.ppij.synth_hero.graphics.piano.GLPiano;
import hr.zpr.ppij.synth_hero.graphics.song.MidiParser;
import hr.zpr.ppij.synth_hero.sounds.SynthesizerPlayer;
import hr.zpr.ppij.synth_hero.tuturial.DescriptionPanel;
import hr.zpr.ppij.synth_hero.tuturial.KeyboardBindings;
import hr.zpr.ppij.synth_hero.tuturial.TutorialPanel;

public class SynthHero {
    
    private static JDialog tutorialDialog = null;
    private static JDialog keyboardDialog = null;
    private static TutorialPanel tutorialPanel = new TutorialPanel();
    private static KeyboardBindings keyBindings = new KeyboardBindings();
    
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignorable) {} // fuck off
        
        // eliminates flickering on resize
        Toolkit.getDefaultToolkit().setDynamicLayout(true);
        System.setProperty("sun.awt.noerasebackground", "true");
        
        SwingUtilities.invokeLater(() -> {
            GLPiano piano = new GLPiano();
            JFrame window = new JFrame("SynthHero");
            JPanel panel = new JPanel(new BorderLayout());
            Animator animator = new Animator(piano);
            
            animator.start();
            
            panel.setDoubleBuffered(true);
            panel.add(createMenu(window, piano), BorderLayout.NORTH);
            panel.add(piano, BorderLayout.CENTER);
            window.addWindowListener(new WindowAdapter() {
                
                @Override
                public void windowClosing(WindowEvent e) {
                    animator.stop();
                }
            });
            window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            window.getContentPane().add(panel, BorderLayout.CENTER);
            window.setSize(640, 480);
            window.setLocationRelativeTo(null);
            window.setVisible(true);
            piano.requestFocusInWindow();
        });
    }
    
    private static JMenuBar createMenu(JFrame window, GLPiano piano) {
        JMenuBar menuBar = new JMenuBar();
        
        // song tab
        JMenu songTab = new JMenu("Song");
        JMenu speedTab = new JMenu("Speed");
        JMenuItem load = new JMenuItem("Load MIDI File");
        JFileChooser chooser = new JFileChooser();
        ButtonGroup speedGroup = new ButtonGroup();
        
        for (int i = 1; i <= 10; i++) {
            JMenuItem speedButton = new JRadioButtonMenuItem(10 * i + "%");
            
            if (i == 10) {
                speedButton.setSelected(true);
            }
            
            speedGroup.add(speedButton);
            speedTab.add(speedButton);
            
            final float percentage = 0.1f * i;
            
            speedButton.addActionListener(action -> {
                piano.setSpeedFactor(percentage);
            });
        }
        
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setFileFilter(new FileNameExtensionFilter("MIDI Files (*.mid)", "mid"));
        
        load.addActionListener(action -> {
            int result = chooser.showOpenDialog(window);
            
            if (result == JFileChooser.APPROVE_OPTION) {
                File file = chooser.getSelectedFile();
                
                loadSong(window, piano, file);
            }
        });
        
        JMenuItem reset = new JMenuItem("Reset");
        
        reset.addActionListener(action -> {
            piano.reset();
        });
        
        songTab.add(load);
        songTab.add(speedTab);
        songTab.add(reset);
        
        // instruments tab
        JMenu instrumentsTab = new JMenu("Instruments");
        JMenu[] instrumentMenus = getInstrumentMenus();
        
        for (JMenu instrumentMenu : instrumentMenus) {
            instrumentsTab.add(instrumentMenu);
        }
        
        String[] instrumentNames = SynthesizerPlayer.getInstrumentNames();
        JMenuItem[] iMenus = new JMenuItem[instrumentNames.length];
        ButtonGroup instrumentsGroup = new ButtonGroup();
        for (int i = 0; i < instrumentMenus.length; i++) {
            for (int j = 0; j < 8; j++) {
                final int index = i * 8 + j;
                
                JMenuItem instrument = new JRadioButtonMenuItem(instrumentNames[i * 8 + j]);
                
                if (i == 0 && j == 0) {
                    instrument.setSelected(true);
                }
                
                instrumentsGroup.add(instrument);
                instrument.addActionListener(action -> {
                    SynthesizerPlayer.setInstrument(index);
                });
                instrumentMenus[i].add(instrument);
                iMenus[index] = instrument;
            }
        }
        
        // input tab
        JMenu inputTab = new JMenu("Input");
        JMenu keyboardTab = new JMenu("Keyboard");
        JMenuItem qwertzBox = new JRadioButtonMenuItem("QWERTZ", true);
        JMenuItem qwertyBox = new JRadioButtonMenuItem("QWERTY");
        JMenuItem keyboardEnabled = new JCheckBoxMenuItem("Enabled", true);
        ButtonGroup keyboardGroup = new ButtonGroup();
        
        keyboardGroup.add(qwertzBox);
        keyboardGroup.add(qwertyBox);
        
        qwertzBox.addActionListener(action -> {
            piano.setQwertzInput();
        });
        qwertyBox.addActionListener(action -> {
            piano.setQwertyInput();
        });
        keyboardEnabled.addActionListener(action -> {
            piano.setKeyboardInput(keyboardEnabled.isSelected());
        });
        
        keyboardTab.add(qwertzBox);
        keyboardTab.add(qwertyBox);
        keyboardTab.add(keyboardEnabled);
        inputTab.add(keyboardTab);
        
        // demo songs tab
        JMenu demoSongsTab = new JMenu("Demo Songs");
        File[] demos = new File[5];
        String resourcesPackage = "resources/";
        
        for (int i = 1; i <= 5; i++) {
            URL url = SynthHero.class.getResource(resourcesPackage + "demo" + i + ".mid");
            try {
                demos[i - 1] = new File(url.toURI());
            } catch (URISyntaxException e) { // goddammit
                e.printStackTrace();
            }
        }
        
        JMenuItem song1 = new JMenuItem("Ludwig van Beethoven - Ode to Joy");
        JMenuItem song2 = new JMenuItem("Ludwig van Beethoven - Für Elise");
        JMenuItem song3 = new JMenuItem("Van Halen - Jump");
        JMenuItem song4 = new JMenuItem("Van Halen - Eruption");
        JMenuItem song5 = new JMenuItem("Kids' music - Old McDonald's Farm");
        
        song1.addActionListener(action -> {
            loadSong(window, piano, demos[0]);
            iMenus[0].setSelected(true);
            SynthesizerPlayer.setInstrument(0);
        });
        song2.addActionListener(action -> {
            loadSong(window, piano, demos[1]);
            iMenus[0].setSelected(true);
            SynthesizerPlayer.setInstrument(0);
        });
        song3.addActionListener(action -> {
            loadSong(window, piano, demos[2]);
            iMenus[63].setSelected(true);
            SynthesizerPlayer.setInstrument(63); // I'm special!
        });
        song4.addActionListener(action -> {
            loadSong(window, piano, demos[3]);
            iMenus[0].setSelected(true);
            SynthesizerPlayer.setInstrument(0);
        });
        song5.addActionListener(action -> {
            loadSong(window, piano, demos[4]);
            iMenus[0].setSelected(true);
            SynthesizerPlayer.setInstrument(0);
        });
        
        demoSongsTab.add(song1);
        demoSongsTab.add(song2);
        demoSongsTab.add(song3);
        demoSongsTab.add(song4);
        demoSongsTab.add(song5);
        
        // help tab
        JMenu helpTab = new JMenu("Help");
        JCheckBoxMenuItem tutorialMode = new JCheckBoxMenuItem("Tutorial Picture", false);
        JMenuItem tutorialDesc = new JMenuItem("Description Tutorial");
        JCheckBoxMenuItem keyboardBindings = new JCheckBoxMenuItem("Keyboard Bindings", false);
        
        tutorialMode.addActionListener(l -> {
            if (tutorialDialog != null) {
                tutorialDialog.setVisible(false);
                tutorialDialog.dispose();
                tutorialDialog = null;
                tutorialMode.setState(false);
                window.setEnabled(true);
                window.toFront();
            } else {
                tutorialDialog = new JDialog();
                tutorialDialog.addWindowListener(new WindowAdapter() {
                    
                    @Override
                    public void windowClosing(WindowEvent windowEvent) {
                        tutorialDialog.setVisible(false);
                        tutorialDialog.dispose();
                        tutorialDialog = null;
                        tutorialMode.setState(false);
                        window.setEnabled(true);
                        window.toFront();
                    }
                });
                tutorialDialog.setSize(1302, 691);
                tutorialDialog.setTitle("Your guide");
                tutorialDialog.getContentPane().add(tutorialPanel);
                tutorialDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                tutorialDialog.setVisible(true);
                window.setEnabled(false);
            }
        });
        
        keyboardBindings.addActionListener(l -> {
            if (keyboardDialog != null) {
                keyboardDialog.setVisible(false);
                keyboardDialog.dispose();
                keyboardDialog = null;
                keyboardBindings.setState(false);
                window.toFront();
            } else {
                keyboardDialog = new JDialog();
                keyboardDialog.addWindowListener(new WindowAdapter() {
                    
                    @Override
                    public void windowClosing(WindowEvent windowEvent) {
                        System.out.println(keyboardDialog.getSize());
                        keyboardDialog.setVisible(false);
                        keyboardDialog.dispose();
                        keyboardDialog = null;
                        keyboardBindings.setState(false);
                        window.toFront();
                    }
                });
                keyboardDialog.getContentPane().add(keyBindings);
                keyboardDialog.setSize(790, 143);
                Point location = window.getLocationOnScreen();
                location.y = location.y + window.getHeight();
                keyboardDialog.setLocation(location);
                keyboardDialog.setTitle("Keyboard Bindings");
                keyboardDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                keyboardDialog.setVisible(true);
                window.toFront();
            }
        });
        
        tutorialDesc.addActionListener(list -> {
            piano.pause();
            JDialog dialog = new JDialog();
            dialog.addWindowListener(new WindowAdapter() {
                
                @Override
                public void windowClosing(WindowEvent windowEvent) {
                    window.setEnabled(true);
                    window.toFront();
                    dialog.dispose();
                }
            });
            dialog.setVisible(true);
            window.setEnabled(false);
            dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            dialog.setLocationRelativeTo(menuBar);
            dialog.setTitle("SynthHero description tutorial");
            dialog.setSize(360, 270);
            dialog.setResizable(false);
            dialog.getContentPane().add(new DescriptionPanel());
            dialog.toFront();
        });
        
        helpTab.add(tutorialMode);
        helpTab.add(tutorialDesc);
        helpTab.add(keyboardBindings);
        
        menuBar.add(songTab);
        menuBar.add(inputTab);
        menuBar.add(instrumentsTab);
        menuBar.add(demoSongsTab);
        menuBar.add(helpTab);
        
        return menuBar;
    }
    
    private static JMenu[] getInstrumentMenus() {
        return new JMenu[] {
                new JMenu("Piano"),
                new JMenu("Chromatic Percussion"),
                new JMenu("Organ"),
                new JMenu("Guitar"),
                new JMenu("Bass"),
                new JMenu("Strings"),
                new JMenu("Ensemble"),
                new JMenu("Brass"),
                new JMenu("Reed"),
                new JMenu("Pipe"),
                new JMenu("Synth Lead"),
                new JMenu("Synth Pad"),
                new JMenu("Synth Effects"),
                new JMenu("Ethnic"),
                new JMenu("Percussive"),
                new JMenu("Sound Effects")
        };
    }
    
    private static void loadSong(JFrame window, GLPiano piano, File file) {
        try {
            piano.loadSong(MidiParser.parseMidiFile(file));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(window, "Unable to open choosen file.", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
}
