package hr.zpr.ppij.synth_hero.tuturial;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import hr.zpr.ppij.synth_hero.SynthHero;

/**
 * Panel for the JDialog of the tutorial
 * 
 * @author Murta
 */
public class TutorialPanel extends JPanel {
    
    private static final long serialVersionUID = 1L;
    
    public TutorialPanel() {
        BufferedImage img = null;
        try {
            String packageName = "tutorial/resources/";
            URL url = SynthHero.class.getResource(packageName + "tut.png");
            
            img = ImageIO.read(url);
        } catch (IOException e) {}
        this.add(new JScrollPane(new JLabel(new ImageIcon(img))));
        this.setVisible(true);
    }
}
