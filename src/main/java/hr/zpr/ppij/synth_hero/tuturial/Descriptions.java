package hr.zpr.ppij.synth_hero.tuturial;
/**
 * Descriptions shown in tutorial. Please look at JavaDoc over desc
 * to implement changes in right way.
 * @author Murta
 *
 */
public abstract class Descriptions {
	
	public static final String tutorialDescription = "This tutorial will describe you"
			+ " how to use SyntHero and have some fun with it.";
	public static final String synthDescription = "While this is not a real-life,"
			+ " synthesizer, it does play sounds and needs coordination like every synth.";
	public static final String keysDescription = "Every synthesizer has two rows of keys,"
			+ " one black and one white and keys are divided in octaves.";
	public static final String activeKeysDescription = "Not all keys are active to play"
			+ " on keyboard. Currently active keys are represented by blue shades.";
	public static final String whiteKeysDescription = "White keys are those which are"
			+ " most often used on synthesizer. There are 7 of them in an octave.";
	public static final String blackKeysDescription = "On the other hand, black keys are"
			+ " rarelier used, each octave contains 5 of those.";
	public static final String butKeysDescription = "But, don't worry about the exact"
			+ " notes, you won't be reading note papers anyway.";
	public static final String butActiveKeysDescription = "But you will have to worry"
			+ " about active keys, pressing LEFT and RIGHT keys on your keyboard to"
			+ " switch to neighboring set as the active one.";
	public static final String playingDescription = "When the note is right above a key,"
			+ " you have to press it. It's just the way it has to be.";
	public static final String keysPlayingDescription = "To see which keys are binded"
			+ " to which note go to help -> keyboard bindings. You can even see it on"
			+ " screen while playing the piano.";
	public static final String keyOrderDescription = "As you see, black keys"
			+ " are over their corresponding white keys, just like on a real synthesizer.";
	public static final String tutorialMode = "Tutorial picture can tell you a lot. More"
			+ " than plain text. So, be sure to look at it and remember some things.";
	public static final String endTutorial = "Are you as sick of this tutorial as I am?"
			+ " Let's get started!";
	/**
	 * Array containing all the descriptions stated above.
	 * Please add in order of presentation.
	 */
	public static final String[] desc = {tutorialDescription, synthDescription,
			keysDescription, activeKeysDescription, whiteKeysDescription,
			blackKeysDescription, butKeysDescription, butActiveKeysDescription,
			keysPlayingDescription, keyOrderDescription, tutorialMode,
			endTutorial};
}