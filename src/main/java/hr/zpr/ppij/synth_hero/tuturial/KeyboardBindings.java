package hr.zpr.ppij.synth_hero.tuturial;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import hr.zpr.ppij.synth_hero.SynthHero;

public class KeyboardBindings extends JPanel {
    
    private static final long serialVersionUID = 1L;
    
    public KeyboardBindings() {
        BufferedImage img = null;
        try {
            String packageName = "tutorial/resources/";
            URL url = SynthHero.class.getResource(packageName + "keys.png");
            
            img = ImageIO.read(url);
        } catch (IOException e) {}
        this.add(new JScrollPane(new JLabel(new ImageIcon(img))));
        this.setVisible(true);
    }
}
