package hr.zpr.ppij.synth_hero.tuturial;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.text.StyleConstants;
/**
 * Panel for the JDialog of the tutorial
 * @author Murta
 *
 */
public class DescriptionPanel extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private int currentSlide = 0;
	private JLabel slideIndicator = new JLabel();
	private JTextPane text = new JTextPane();
	private JButton previous = new JButton("Previous");
	private JButton next = new JButton("Next");
	
	public DescriptionPanel(){
		this.setLayout(new BorderLayout());

		//buttons
		JPanel buttonsPanel = new JPanel();
		previous.setEnabled(false);
		previous.addActionListener(l ->{
			if(currentSlide > 0){
				currentSlide--;
				next.setEnabled(true);
			}
			else{
				previous.setEnabled(false);
			}
			if(currentSlide == 0) previous.setEnabled(false);
			text.setText(Descriptions.desc[currentSlide]);
			slideIndicator.setText(currentSlide+1+"/"+Descriptions.desc.length);
		});
		next.addActionListener(l ->{
			if(currentSlide < Descriptions.desc.length-1){
				currentSlide++;
				previous.setEnabled(true);
			}
			if(currentSlide == Descriptions.desc.length-1) next.setEnabled(false);
			text.setText(Descriptions.desc[currentSlide]);
			slideIndicator.setText(currentSlide+1+"/"+Descriptions.desc.length);
		});
		buttonsPanel.add(previous);
		buttonsPanel.add(next);
		this.add(buttonsPanel, BorderLayout.SOUTH);
		
		//JTextPane
		this.add(text, BorderLayout.CENTER);
		StyleConstants.setForeground(text.getInputAttributes(), Color.WHITE);
		text.setText(Descriptions.desc[currentSlide]);
		text.setEditable(false);
		text.setFont(text.getFont().deriveFont(24f));
		text.setBackground(Color.BLACK);
		
		//indicator
		slideIndicator.setBackground(Color.GREEN);
		this.add(slideIndicator, BorderLayout.NORTH);
		slideIndicator.setText(currentSlide+1+"/"+Descriptions.desc.length);
		slideIndicator.setHorizontalAlignment(SwingConstants.CENTER);
		
		this.setVisible(true);
	}
	
}
