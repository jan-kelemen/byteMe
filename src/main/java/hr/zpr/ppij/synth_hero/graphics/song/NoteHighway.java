package hr.zpr.ppij.synth_hero.graphics.song;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2GL3;
import hr.zpr.ppij.synth_hero.graphics.FloatColor;

public class NoteHighway {
    
    private float x;
    private float y;
    private float width;
    private float height;
    private int currentNote;
    private boolean active;
    private boolean selected;
    private List<Note> notes;
    private final FloatColor color;
    private final FloatColor borderColor;
    private final FloatColor activeColor;
    private final FloatColor activeBorderColor;
    
    public NoteHighway(Color color, Color activeColor) {
        this.color = new FloatColor(color);
        this.borderColor = new FloatColor(this.color, 0.3f);
        this.activeColor = new FloatColor(activeColor);
        this.activeBorderColor = new FloatColor(this.activeColor, 0.3f);
        
        notes = new ArrayList<>();
    }
    
    public void draw(GL2 gl2, long currentTime) {
        int lastNote = currentNote;
        
        active = false;
        
        while (currentNote != notes.size()) {
            Note note = notes.get(currentNote++);
            
            long startTime = note.getStartTime() - currentTime;
            
            if (startTime >= 5000L) {
                currentNote--;
                break;
            }
            
            long endTime = note.getEndTime() - currentTime;
            
            if (endTime <= 0) {
                lastNote = currentNote;
            }
            
            float offsetStartY = y + height * startTime / 5000.0f;
            float offsetEndY = y + height * endTime / 5000.0f;
            boolean thisActive = false;
            
            if (startTime <= 0L && endTime >= 0L && selected) {
                active = true;
                thisActive = true;
            }
            
            if (offsetStartY < y) {
                offsetStartY = y;
            }
            
            if (endTime > 0) {
                gl2.glBegin(GL2GL3.GL_QUADS);
                
                if (thisActive) {
                    gl2.glColor3f(activeColor.getRed(), activeColor.getGreen(), activeColor.getBlue());
                } else {
                    gl2.glColor3f(color.getRed(), color.getGreen(), color.getBlue());
                }
                
                gl2.glVertex2f(x, offsetStartY);
                gl2.glVertex2f(x + width, offsetStartY);
                gl2.glVertex2f(x + width, offsetEndY);
                gl2.glVertex2f(x, offsetEndY);
                gl2.glEnd();
                gl2.glBegin(GL.GL_LINE_STRIP);
                
                if (thisActive) {
                    gl2.glColor3f(activeBorderColor.getRed(), activeBorderColor.getGreen(),
                            activeBorderColor.getBlue());
                } else {
                    gl2.glColor3f(borderColor.getRed(), borderColor.getGreen(), borderColor.getBlue());
                }
                
                gl2.glVertex2f(x, offsetStartY);
                gl2.glVertex2f(x + width, offsetStartY);
                gl2.glVertex2f(x + width, offsetEndY);
                gl2.glVertex2f(x, offsetEndY);
                gl2.glEnd();
            }
        }
        
        currentNote = lastNote;
    }
    
    public void relocate(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    public void select() {
        selected = true;
    }
    
    public void deselect() {
        selected = false;
    }
    
    public boolean isActive() {
        return active;
    }
    
    public void addNote(Note note) {
        notes.add(note);
    }
    
    public void reset() {
        currentNote = 0;
    }
    
    public void clear() {
        reset();
        notes.clear();
    }
}
