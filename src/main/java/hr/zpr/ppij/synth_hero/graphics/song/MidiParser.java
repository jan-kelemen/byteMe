package hr.zpr.ppij.synth_hero.graphics.song;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

/**
 * Created by skux on 18/05/16.
 */
public class MidiParser {
    
    // nema smisla da je objekt, bolje je static
    // YOU HAD ONE JOB!! nvm, no h8 m8
    public static SortedSet<Note> parseMidiFile(File midiFile) throws Exception {
        SortedSet<Note> parsedNotes = new TreeSet<>();
        Map<Integer, Long> onNotes = new HashMap<>();
        Sequence sequence = MidiSystem.getSequence(midiFile);
        
        long ticks = sequence.getTickLength();
        long micros = sequence.getMicrosecondLength();
        double microsPerTick = (double) micros / ticks;
        for (Track track : sequence.getTracks()) {
            for (int i = 0, size = track.size(); i < size; i++) {
                MidiEvent currentEvent = track.get(i);
                MidiMessage message = currentEvent.getMessage();
                
                long currentTimestamp = (long) (currentEvent.getTick() * microsPerTick) / 1000L;
                byte[] messageBytes = message.getMessage();
                int command = messageBytes[0] & 0xF0;
                
                if (command == ShortMessage.NOTE_ON) {
                    for (int j = 1; j < messageBytes.length; j += 2) {
                        int note = messageBytes[j] & 0xFF;
                        int velocity = messageBytes[j + 1] & 0xFF;
                        
                        onNotes.compute(note, (key, value) -> {
                            if (value == null) {
                                return currentTimestamp;
                            } else if (velocity == 0) {
                                parsedNotes.add(new Note(key, value, currentTimestamp));
                                
                                return null;
                            } else {
                                return value;
                            }
                        });
                    }
                } else if (command == ShortMessage.NOTE_OFF) {
                    for (int j = 1; j < messageBytes.length; j += 2) {
                        int note = messageBytes[j] & 0xFF;
                        
                        if (onNotes.containsKey(note)) {
                            parsedNotes.add(new Note(note, onNotes.get(note), currentTimestamp));
                            onNotes.remove(note);
                        }
                    }
                }
            }
        }
        
        return parsedNotes;
    }
    
}
