package hr.zpr.ppij.synth_hero.graphics.song;

public class Note implements Comparable<Note> {
    
    private int midiValue;
    private long startTime;
    private long endTime;
    
    public Note(int midiValue, long startTime, long endTime) {
        this.midiValue = midiValue;
        this.startTime = startTime;
        this.endTime = endTime;
    }
    
    public int getMidiValue() {
        return midiValue;
    }
    
    public void setMidiValue(int midiValue) {
        this.midiValue = midiValue;
    }
    
    public long getStartTime() {
        return startTime;
    }
    
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
    
    public long getEndTime() {
        return endTime;
    }
    
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
    
    @Override
    public int compareTo(final Note o) {
        if (startTime <= o.startTime) {
            return -1;
        } else {
            return 1; // musn't be 0. otherwise sets won't be happy.
        }
    }
}
