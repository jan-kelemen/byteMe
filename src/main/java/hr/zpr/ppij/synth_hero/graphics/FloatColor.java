package hr.zpr.ppij.synth_hero.graphics;

import java.awt.Color;

/**
 * Used to store info about color in float format.
 * 
 * @author Domagoj Latečki
 * @version 1.0
 * @since 1.0
 */
public class FloatColor {
    
    private final float red;
    private final float green;
    private final float blue;
    
    public FloatColor(Color color) {
        red = color.getRed() / 255.0f;
        green = color.getGreen() / 255.0f;
        blue = color.getBlue() / 255.0f;
    }
    
    public FloatColor(FloatColor color, float scaleFactor) {
        red = color.getRed() * scaleFactor;
        green = color.getGreen() * scaleFactor;
        blue = color.getBlue() * scaleFactor;
    }
    
    public float getRed() {
        return red;
    }
    
    public float getGreen() {
        return green;
    }
    
    public float getBlue() {
        return blue;
    }
}
