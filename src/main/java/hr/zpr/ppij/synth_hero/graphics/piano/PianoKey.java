package hr.zpr.ppij.synth_hero.graphics.piano;

import java.awt.Color;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2GL3;
import hr.zpr.ppij.synth_hero.graphics.FloatColor;
import hr.zpr.ppij.synth_hero.graphics.song.Note;
import hr.zpr.ppij.synth_hero.graphics.song.NoteHighway;
import hr.zpr.ppij.synth_hero.sounds.SynthesizerPlayer;

/**
 * A single key of a piano.
 * 
 * @author Domagoj Latečki
 * @version 1.0
 * @since 1.0
 */
public class PianoKey {
    
    private int midiNote;
    private float x;
    private float y;
    private float width;
    private float height;
    private volatile boolean highlighted;
    private volatile boolean selected;
    private final NoteHighway highway;
    private final FloatColor color;
    private final FloatColor highlight;
    private final FloatColor activeColor;
    private final FloatColor selectedColor;
    
    public PianoKey(Color color, Color highlight, Color selectedColor, Color activeColor) {
        this.highway = new NoteHighway(highlight, activeColor);
        this.color = new FloatColor(color);
        this.highlight = new FloatColor(highlight);
        this.activeColor = new FloatColor(activeColor);
        this.selectedColor = new FloatColor(selectedColor);
    }
    
    public void drawHighway(GL2 gl2, long currentTime) {
        highway.draw(gl2, currentTime);
    }
    
    public void draw(GL2 gl2, long currentTime) {
        highway.draw(gl2, currentTime);
        gl2.glBegin(GL2GL3.GL_QUADS);
        if (highway.isActive()) {
            gl2.glColor3f(activeColor.getRed(), activeColor.getGreen(), activeColor.getBlue());
        } else if (highlighted) {
            gl2.glColor3f(highlight.getRed(), highlight.getGreen(), highlight.getBlue());
        } else if (selected) {
            gl2.glColor3f(selectedColor.getRed(), selectedColor.getGreen(), selectedColor.getBlue());
        } else {
            gl2.glColor3f(color.getRed(), color.getGreen(), color.getBlue());
        }
        gl2.glVertex2f(x, y);
        gl2.glVertex2f(x + width, y);
        gl2.glVertex2f(x + width, y + height);
        gl2.glVertex2f(x, y + height);
        gl2.glEnd();
        gl2.glBegin(GL.GL_LINE_STRIP);
        gl2.glColor3f(0.0f, 0.0f, 0.0f);
        gl2.glVertex2f(x, y);
        gl2.glVertex2f(x + width, y);
        gl2.glVertex2f(x + width, y + height);
        gl2.glVertex2f(x, y + height);
        gl2.glEnd();
    }
    
    public void highlight() {
        highway.select();
        if (!highlighted) {
            SynthesizerPlayer.noteOn(midiNote, 127);
        }
        highlighted = true;
    }
    
    public void normalize() {
        highway.deselect();
        highlighted = false;
        SynthesizerPlayer.noteOff(midiNote, 127);
    }
    
    public void relocate(float x, float y, float width, float height, float highwayHeight) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        
        highway.relocate(x, y + height, width, highwayHeight);
    }
    
    public void select() {
        selected = true;
    }
    
    public void deselect() {
        selected = false;
    }
    
    public void addNote(Note note) {
        highway.addNote(note);
    }
    
    public void setMidiNote(int midiNote) {
        this.midiNote = midiNote;
    }
    
    public void reset() {
        highway.reset();
    }
    
    public void clear() {
        highway.clear();
    }
}
