package hr.zpr.ppij.synth_hero.graphics.song;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2GL3;

/**
 * Progress bar of the song.
 * 
 * @author Domagoj Latečki
 * @version 1.0
 * @since 1.0
 */
public class ProgressBar {
    
    private int x;
    private int y;
    private int width;
    private int height;
    private long currentProgress = 0L;
    private long expectedProgress = -1L;
    private boolean paused = true;
    
    public void draw(GL2 gl2) {
        gl2.glBegin(GL2GL3.GL_QUADS);
        gl2.glColor3f(0.25f, 0.25f, 0.25f);
        gl2.glVertex2i(x, y);
        gl2.glVertex2i(x + width, y);
        gl2.glVertex2i(x + width, y + height);
        gl2.glVertex2i(x, y + height);
        gl2.glColor3f(0.27f, 0.93f, 0.5f);
        gl2.glVertex2i(height + x, y);
        gl2.glVertex2i((int) (height + x + (width - height) * (float) currentProgress / expectedProgress), y);
        gl2.glVertex2i((int) (height + x + (width - height) * (float) currentProgress / expectedProgress), y + height);
        gl2.glVertex2i(height + x, y + height);
        gl2.glEnd();
        gl2.glBegin(GL.GL_LINES);
        gl2.glColor3f(0.15f, 0.15f, 0.15f);
        gl2.glVertex2i(x, y);
        gl2.glVertex2i(x + width, y);
        gl2.glVertex2i(x + height, y);
        gl2.glVertex2i(x + height, y + height);
        gl2.glEnd();
        
        float fifth = height / 5.0f;
        float offset = height - fifth;
        if (paused) {
            float twoFifths = fifth * 2.0f;
            
            gl2.glBegin(GL2GL3.GL_QUADS);
            gl2.glVertex2f(x + fifth, y + fifth);
            gl2.glVertex2f(x + twoFifths, y + fifth);
            gl2.glVertex2f(x + twoFifths, y + offset);
            gl2.glVertex2f(x + fifth, y + offset);
            gl2.glVertex2f(x + offset - fifth, y + fifth);
            gl2.glVertex2f(x + offset, y + fifth);
            gl2.glVertex2f(x + offset, y + offset);
            gl2.glVertex2f(x + offset - fifth, y + offset);
            
        } else {
            gl2.glBegin(GL.GL_TRIANGLES);
            gl2.glVertex2f(x + fifth, y + fifth);
            gl2.glVertex2f(x + offset, y + height / 2.0f);
            gl2.glVertex2f(x + fifth, y + offset);
        }
        gl2.glEnd();
    }
    
    public void relocate(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    public void togglePause() {
        paused = !paused;
    }
    
    public void setProgress(long progress) {
        if (expectedProgress == -1L) {
            return;
        }
        
        if (progress < 0L) {
            currentProgress = 0L;
        } else if (progress > expectedProgress) {
            currentProgress = expectedProgress;
        } else {
            currentProgress = progress;
        }
    }
    
    public void setExpectedProgress(long expectedProgress) {
        this.expectedProgress = expectedProgress;
    }
    
    public void reset() {
        currentProgress = 0L;
    }
}
