package hr.zpr.ppij.synth_hero.graphics.piano;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.function.Consumer;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;
import hr.zpr.ppij.synth_hero.graphics.song.Note;
import hr.zpr.ppij.synth_hero.graphics.song.ProgressBar;

/**
 * Canvas which contains a playable piano and note highway.
 * 
 * @author Domagoj Latečki
 * @version 1.0
 * @since 1.0
 */
public class GLPiano extends GLCanvas {
    
    /**
     * Won't be used.
     */
    private static final long serialVersionUID = 1L;
    
    private final PianoOctave[] octaves = {
            new PianoOctave(-3),
            new PianoOctave(),
            new PianoOctave(),
            new PianoOctave(),
            new PianoOctave(),
            new PianoOctave(),
            new PianoOctave(),
            new PianoOctave(),
            new PianoOctave(1)
    };
    
    private int zKey = KeyEvent.VK_Z;
    private int yKey = KeyEvent.VK_Y;
    private int selectedOctave;
    private float speedFactor = 1.0f;
    private long lastTime = System.currentTimeMillis();
    private long currentTime = -2500L;
    private volatile boolean keyboardInput = true;
    private volatile boolean paused = true;
    private final PianoKey[] selectedKeys = new PianoKey[29];
    private final ProgressBar progressBar = new ProgressBar();
    private final Map<Integer, PianoKey> midiNoteToKey = new HashMap<>();
    
    public GLPiano() {
        super(getCapabilities());
        
        int i = 21;
        for (PianoOctave octave : octaves) {
            for (int j = 0; j < 12; j++) {
                PianoKey key = octave.getKey(j);
                
                if (key != null) {
                    midiNoteToKey.put(i++, key);
                    key.setMidiNote(i);
                }
            }
        }
        
        selectedOctave = 3;
        
        addKeyboardListener();
        addGLEventListener();
        selectKeys(selectedOctave);
    }
    
    private void updateTime() {
        long time = System.currentTimeMillis();
        long update = time - lastTime;
        
        currentTime += update * speedFactor;
        progressBar.setProgress(currentTime);
        lastTime = time;
    }
    
    private void togglePause() {
        progressBar.togglePause();
        
        if (paused) {
            lastTime = System.currentTimeMillis();
        }
        
        paused = !paused;
    }
    
    /*
     * Added this method to stop the piano for tutorial.
     * @author Murta
     */
    public void pause() {
        if (!paused) {
            togglePause();
        }
    }
    
    /*
     * Added this method to resume the piano for tutorial.
     * @author Murta
     */
    public void unpause() {
        if (paused) {
            togglePause();
        }
    }
    
    private void rewind(long value) {
        currentTime -= value;
        
        for (PianoKey key : midiNoteToKey.values()) {
            key.reset();
        }
        
        progressBar.setProgress(currentTime);
    }
    
    private void fastForward(long value) {
        currentTime += value;
        progressBar.setProgress(currentTime);
    }
    
    private static GLCapabilities getCapabilities() {
        GLCapabilities capabilities = new GLCapabilities(GLProfile.getDefault());
        
        capabilities.setDoubleBuffered(true);
        
        return capabilities;
    }
    
    private void addKeyboardListener() {
        addKeyListener(new KeyAdapter() {
            
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                
                if (keyCode == KeyEvent.VK_SPACE) {
                    togglePause();
                } else if (keyCode == KeyEvent.VK_UP) {
                    fastForward(1000L);
                } else if (keyCode == KeyEvent.VK_DOWN) {
                    rewind(1000L);
                } else if (keyboardInput) {
                    if (keyCode == KeyEvent.VK_LEFT) {
                        selectOctave(selectedOctave - 1);
                        selectKeys(selectedOctave);
                    } else if (keyCode == KeyEvent.VK_RIGHT) {
                        selectOctave(selectedOctave + 1);
                        selectKeys(selectedOctave);
                    } else {
                        keyAction(keyCode, GLPiano.this::pressKey);
                    }
                }
                
                display();
            }
            
            @Override
            public void keyReleased(KeyEvent e) {
                keyAction(e.getKeyCode(), GLPiano.this::releaseKey);
                display();
            }
        });
    }
    
    private void addGLEventListener() {
        addGLEventListener(new GLEventListener() {
            
            @Override
            public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
                
                GL2 gl2 = drawable.getGL().getGL2();
                
                gl2.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
                gl2.glLoadIdentity();
                
                GLU glu = new GLU();
                
                glu.gluOrtho2D(0.0f, width, 0.0f, height);
                gl2.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
                gl2.glLoadIdentity();
                gl2.glViewport(0, 0, width, height);
                gl2.glLineWidth(2.0f);
                
                boolean first = true;
                float keyWidth = width / 52.0f; // 52 white keys
                float octaveWidth = 7.0f * keyWidth;
                float octaveHeight = height * 0.25f;
                float highwayHeight = height * 0.7f;
                float offset = 0.0f;
                
                for (PianoOctave octave : octaves) {
                    octave.relocate(x + offset, y, octaveWidth, octaveHeight, highwayHeight);
                    if (first) {
                        first = false;
                        offset += 2.0f * keyWidth;
                    } else {
                        offset += octaveWidth;
                    }
                }
                
                progressBar.relocate(x, (int) Math.ceil(height * 0.95f), width, (int) Math.ceil(height * 0.05f));
            }
            
            @Override
            public void init(GLAutoDrawable drawable) {}
            
            @Override
            public void dispose(GLAutoDrawable drawable) {}
            
            @Override
            public void display(GLAutoDrawable drawable) {
                GL2 gl2 = drawable.getGL().getGL2();
                
                int width = drawable.getSurfaceWidth();
                int height = drawable.getSurfaceHeight();
                
                gl2.glClear(GL.GL_COLOR_BUFFER_BIT);
                gl2.glLoadIdentity();
                gl2.glColor3f(0.1f, 0.1f, 0.1f);
                gl2.glBegin(GL2GL3.GL_QUADS);
                gl2.glVertex2i(0, 0);
                gl2.glVertex2i(width, 0);
                gl2.glVertex2i(width, height);
                gl2.glVertex2i(0, height);
                gl2.glEnd();
                
                for (PianoOctave octave : octaves) {
                    octave.draw(gl2, currentTime);
                }
                
                progressBar.draw(gl2);
                
                gl2.glFlush();
                
                if (!paused) {
                    updateTime();
                }
            }
        });
    }
    
    private void selectOctave(int index) {
        if (index > 6) {
            selectedOctave = 6;
        } else if (index < 0) {
            selectedOctave = 0;
        } else {
            selectedOctave = index;
        }
    }
    
    private void selectKeys(int firstOctave) {
        int secondOctave = firstOctave + 1;
        int thirdOctave;
        if (firstOctave == 6) {
            thirdOctave = -1;
        } else {
            thirdOctave = firstOctave + 2;
        }
        
        for (PianoKey key : selectedKeys) {
            if (key != null) {
                key.deselect();
                key.normalize();
            }
        }
        
        int clearFirst = firstOctave == 0 ? 9 : 0;
        int i = 0;
        
        for (; i < clearFirst; i++) {
            selectedKeys[i] = null;
        }
        
        for (; i <= 11; i++) {
            PianoKey key = octaves[firstOctave].getKey(i);
            
            key.select();
            selectedKeys[i] = key;
        }
        
        for (i = 12; i <= 23; i++) {
            PianoKey key = octaves[secondOctave].getKey(i - 12);
            
            key.select();
            selectedKeys[i] = key;
        }
        
        if (thirdOctave != -1) {
            for (i = 24; i <= 28; i++) {
                PianoKey key = octaves[thirdOctave].getKey(i - 24);
                
                key.select();
                selectedKeys[i] = key;
            }
        } else {
            PianoKey key = octaves[8].getKey(0);
            
            key.select();
            selectedKeys[24] = key;
            
            for (i = 25; i <= 28; i++) {
                selectedKeys[i] = null;
            }
        }
    }
    
    private void keyAction(int keyCode, Consumer<Integer> action) {
        if (keyCode == yKey) {
            action.accept(0);
        } else if (keyCode == zKey) {
            action.accept(21);
        } else {
            switch (keyCode) {
                case KeyEvent.VK_S:
                    action.accept(1);
                    break;
                case KeyEvent.VK_X:
                    action.accept(2);
                    break;
                case KeyEvent.VK_D:
                    action.accept(3);
                    break;
                case KeyEvent.VK_C:
                    action.accept(4);
                    break;
                case KeyEvent.VK_V:
                    action.accept(5);
                    break;
                case KeyEvent.VK_G:
                    action.accept(6);
                    break;
                case KeyEvent.VK_B:
                    action.accept(7);
                    break;
                case KeyEvent.VK_H:
                    action.accept(8);
                    break;
                case KeyEvent.VK_N:
                    action.accept(9);
                    break;
                case KeyEvent.VK_J:
                    action.accept(10);
                    break;
                case KeyEvent.VK_M:
                    action.accept(11);
                    break;
                case KeyEvent.VK_Q:
                    action.accept(12);
                    break;
                case KeyEvent.VK_2:
                    action.accept(13);
                    break;
                case KeyEvent.VK_W:
                    action.accept(14);
                    break;
                case KeyEvent.VK_3:
                    action.accept(15);
                    break;
                case KeyEvent.VK_E:
                    action.accept(16);
                    break;
                case KeyEvent.VK_R:
                    action.accept(17);
                    break;
                case KeyEvent.VK_5:
                    action.accept(18);
                    break;
                case KeyEvent.VK_T:
                    action.accept(19);
                    break;
                case KeyEvent.VK_6:
                    action.accept(20);
                    break;
                case KeyEvent.VK_7:
                    action.accept(22);
                    break;
                case KeyEvent.VK_U:
                    action.accept(23);
                    break;
                case KeyEvent.VK_I:
                    action.accept(24);
                    break;
                case KeyEvent.VK_9:
                    action.accept(25);
                    break;
                case KeyEvent.VK_O:
                    action.accept(26);
                    break;
                case KeyEvent.VK_0:
                    action.accept(27);
                    break;
                case KeyEvent.VK_P:
                    action.accept(28);
                    break;
                default:
                    break;
            }
        }
    }
    
    private void pressKey(int index) {
        if (selectedKeys[index] != null) {
            selectedKeys[index].highlight();
        }
    }
    
    private void releaseKey(int index) {
        if (selectedKeys[index] != null) {
            selectedKeys[index].normalize();
        }
    }
    
    public void setKeyboardInput(boolean keyboardInput) {
        this.keyboardInput = keyboardInput;
        
        if (keyboardInput) {
            for (PianoKey key : selectedKeys) {
                if (key != null) {
                    key.select();
                }
            }
        } else {
            for (PianoKey key : selectedKeys) {
                if (key != null) {
                    key.deselect();
                }
            }
        }
        
        display();
    }
    
    public void loadSong(SortedSet<Note> notes) {
        clearSong();
        notes.stream().forEach(note -> {
            addNote(note.getMidiValue(), note.getStartTime(), note.getEndTime());
        });
        progressBar.setExpectedProgress(notes.last().getEndTime() + 2500L);
    }
    
    private void addNote(int midiNote, long startTime, long endTime) {
        PianoKey key = midiNoteToKey.get(midiNote);
        
        if (key != null) {
            key.addNote(new Note(midiNote, startTime, endTime));
        }
    }
    
    public void clearSong() {
        reset();
        
        for (PianoKey key : midiNoteToKey.values()) {
            key.clear();
        }
    }
    
    public void reset() {
        pause();
        progressBar.reset();
        lastTime = System.currentTimeMillis();
        currentTime = -2500L;
        
        for (PianoKey key : midiNoteToKey.values()) {
            key.reset();
        }
    }
    
    public void setQwertyInput() {
        yKey = KeyEvent.VK_Z;
        zKey = KeyEvent.VK_Y;
    }
    
    public void setQwertzInput() {
        zKey = KeyEvent.VK_Z;
        yKey = KeyEvent.VK_Y;
    }
    
    public void setSpeedFactor(float speedFactor) {
        this.speedFactor = speedFactor;
    }
}
