package hr.zpr.ppij.synth_hero.graphics.piano;

import java.awt.Color;
import java.util.function.Function;
import com.jogamp.opengl.GL2;

/**
 * A single octave of a piano.
 * 
 * @author Domagoj Latečki
 * @version 1.0
 * @since 1.0
 */
public class PianoOctave {
    
    private static final int[] KEY_INDEXES = { 0, 7, 1, 8, 2, 3, 9, 4, 10, 5, 11, 6 };
    private static final Color HIGHLIGHT_LIGHT = new Color(70, 128, 238);
    private static final Color SELECTED_LIGHT = new Color(199, 212, 237);
    private static final Color ACTIVE_COLOR_LIGHT = new Color(70, 238, 128);
    private static final Color HIGHLIGHT_DARK = new Color(35, 64, 119);
    private static final Color SELECTED_DARK = new Color(6, 12, 24);
    private static final Color ACTIVE_COLOR_DARK = new Color(35, 119, 64);
    private final PianoKey[] keys;
    
    public PianoOctave() {
        this(12);
    }
    
    public PianoOctave(int numKeys) {
        keys = new PianoKey[12];
        
        for (int i = 0; i <= 6; i++) {
            keys[i] = new PianoKey(Color.WHITE, HIGHLIGHT_LIGHT, SELECTED_LIGHT, ACTIVE_COLOR_LIGHT);
        }
        
        for (int i = 7; i <= 11; i++) {
            keys[i] = new PianoKey(Color.BLACK, HIGHLIGHT_DARK, SELECTED_DARK, ACTIVE_COLOR_DARK);
        }
        
        int init;
        Function<Integer, Boolean> checker;
        
        if (numKeys >= 0) {
            init = numKeys;
            checker = i -> i < keys.length;
        } else {
            init = 0;
            checker = i -> i < keys.length + numKeys;
        }
        
        for (int i = init; checker.apply(i); i++) {
            keys[KEY_INDEXES[i]] = null;
        }
    }
    
    public void draw(GL2 gl2, long currentTime) {
        for (PianoKey key : keys) {
            if (key != null) {
                key.draw(gl2, currentTime);
            }
        }
    }
    
    public PianoKey getKey(int index) {
        return keys[KEY_INDEXES[index]];
    }
    
    public void relocate(float x, float y, float width, float height, float highwayHeight) {
        float keyWidth = width / 7.0f;
        float blackKeyWidth = keyWidth / 1.5f;
        float blackKeyXOffset = keyWidth - blackKeyWidth / 2.0f;
        float blackKeyHeight = height / 1.75f;
        float blackKeyYOffset = y + height - blackKeyHeight;
        
        int keyIndex = 0;
        for (int i = 0; i <= 6; i++) {
            if (keys[i] != null) {
                keys[i].relocate(x + keyIndex * keyWidth, y, keyWidth, height, highwayHeight);
                keyIndex++;
            }
        }
        
        keyIndex = 7;
        
        int offsetFactor = 7;
        for (int i = 7; i <= 11; i++) {
            if (keyIndex == 9) {
                offsetFactor -= 1;
            }
            
            if (keys[i] != null) {
                keys[i].relocate(x + blackKeyXOffset + (keyIndex - offsetFactor) * keyWidth, blackKeyYOffset,
                        blackKeyWidth, blackKeyHeight, highwayHeight);
                keyIndex++;
            }
        }
    }
}
