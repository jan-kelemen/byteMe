package hr.zpr.ppij.synth_hero.sounds;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;

/**
 * <code>SynthesizerPlayer</code> .
 *
 * @author Jan Kelemen
 * @version alpha
 */
public class SynthesizerPlayer {
    
    private static final Synthesizer SYNTHESIZER;
    private static final MidiChannel[] CHANNELS;
    private static final MidiChannel CURRENT_CHANNEL;
    
    static {
        try {
            SYNTHESIZER = MidiSystem.getSynthesizer();
            SYNTHESIZER.open();
            
            Soundbank sb = SYNTHESIZER.getDefaultSoundbank();
            
            if (sb != null) {
                SYNTHESIZER.loadInstrument(SYNTHESIZER.getDefaultSoundbank().getInstruments()[0]);
            }
            
            CHANNELS = SYNTHESIZER.getChannels();
            CURRENT_CHANNEL = CHANNELS[0];
        } catch (MidiUnavailableException e) {
            throw new RuntimeException("Unable to load synth.");
        }
    }
    
    public static void noteOn(int noteNumber, int velocity) {
        CURRENT_CHANNEL.noteOn(noteNumber, velocity);
    }
    
    public static void noteOff(int noteNumber, int velocity) {
        CURRENT_CHANNEL.noteOff(noteNumber, velocity);
    }
    
    public static MidiChannel getCurrentChannel() {
        return CURRENT_CHANNEL;
    }
    
    public static void setInstrument(int index) {
        SYNTHESIZER.loadInstrument(SYNTHESIZER.getDefaultSoundbank().getInstruments()[index]);
        CURRENT_CHANNEL.programChange(index);
    }
    
    public static String[] getInstrumentNames() {
        Instrument[] instruments = SYNTHESIZER.getDefaultSoundbank().getInstruments();
        String[] output = new String[instruments.length];
        
        for (int i = 0; i < instruments.length; i++) {
            output[i] = instruments[i].getName();
        }
        
        return output;
    }
}
